# BubbleSort

Implementation of BubbleSort algorithm.

## Description (from wiki)

Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the input list element by element, comparing the current element with the one after it, swapping their values if needed. These passes through the list are repeated until no swaps had to be performed during a pass, meaning that the list has become fully sorted. The algorithm, which is a comparison sort, is named for the way the larger elements "bubble" up to the top of the list.

## Short video

https://www.youtube.com/watch?v=8-dxXugUS8Y
