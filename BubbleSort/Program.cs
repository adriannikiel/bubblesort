﻿using System;

namespace BubbleSort
{
    class Program
    {
        public static int[] test1 = { };
        public static int[] test2 = { 1, 2, 5, 3, 1, 7, 9, 1, 12, 83, 1, 5, 3, 2 };
        public static int[] test3 = { 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1 };
        public static int[] test4 = { 2, 2, 2, 2 };
        public static int[] test5 = { 1, 2 };
        public static int[] test6 = { 2, 1 };

        static void Main(string[] args)
        {
            //runTest(test1);
            runTest(test2);
            runTest(test3);
            runTest(test4);
            runTest(test5);
            runTest(test6);

        }

        private static void runTest(int[] testArray)
        {
            Console.WriteLine("\n----------");
            Console.WriteLine("Before sorting:");
            displayArray(testArray);
            Console.ReadLine();

            BubbleSortAlgorithm.sort(testArray);

            Console.WriteLine("\nAfter sorting:");
            displayArray(testArray);
            Console.ReadLine();
        }

        private static void displayArray(int[] numberArray)
        {
            foreach (int number in numberArray)
            {
                Console.Write(number + " ");
            }
        }

    }

    class BubbleSortAlgorithm
    {
        public static void sort(int[] numberArray)
        {
            int n = numberArray.Length;

            do
            {
                for (int i = 0; i < n - 1; i++)
                {
                    if (numberArray[i] > numberArray[i + 1])
                    {
                        int temp = numberArray[i];
                        numberArray[i] = numberArray[i + 1];
                        numberArray[i + 1] = temp;
                    }
                }

                n = n - 1;

            } while (n > 1);
        }
    }
}
